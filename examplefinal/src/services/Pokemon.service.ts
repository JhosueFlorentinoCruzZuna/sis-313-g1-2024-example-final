import { instancePokemon } from "@/configuration/configPokemon"

export const getPokemon = async () => {
    //https://pokeapi.co/api/v2/pokemon?limit=5&offset=0
    const response = await instancePokemon.get('/pokemon?limit=9&offset=12')    
    return response.data
}
