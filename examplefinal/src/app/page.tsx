"use client"
import Image from "next/image";
import styles from "./page.module.css";
import { Nav } from "@/componets/Nav";
import { Box, Grid } from "@mui/material";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import CardOne from "@/componets/CardOne";
import CardTwo from "@/componets/CardTwo";
import {List} from "@/componets/List";
import CardTree from "@/componets/CardTree";
import CardFour from "@/componets/CardFour";
import Link from "next/link";
import { Footer } from "@/componets/Footer";
import { usePokemon } from "@/hooks/usePokemon";
export default function Home() {
  const { pokemon } = usePokemon()
  return (
    <Box className="font">
      <Nav></Nav>
      <Box sx={{display:"flex", justifyContent:"center", alignItems:"center", flexDirection:"column", padding:"100px 300px", gap:"20px"}}>
      <Typography className="pa" variant="h4" gutterBottom>
      Start and Build Your Crypto Portfolio Here
      </Typography>
      <Typography className="pa" variant="body2" gutterBottom sx={{padding:"0 160px", textAlign:"center"}}>
      Only at CryptoCap, you can build a good portfolio and learn 
      best practices about cryptocurrency.
      </Typography>
      <Button sx={{background:"#0FAE96"}} variant="contained">Get Started</Button>
      </Box>
      <Typography className="pa" sx={{ margin:"20px 60px"}} variant="h4"> Market Trend</Typography>
      <Box sx={{display:"flex", justifyContent:"space-evenly", padding:"0 60px"}}>
        <CardOne></CardOne>
        <CardOne></CardOne>
        <CardOne></CardOne>
        <CardOne></CardOne>
      </Box>
      <Box sx={{padding:"120px 60px 50px 60px", display:"flex", textAlign:"center", flexDirection:"column"}}>
      <Typography className="pa" variant="h4" gutterBottom>
      CryptoCap Amazing Faetures
      </Typography>
      <Typography className="pa" variant="body1" gutterBottom>
      Explore sensational features to prepare your best investment in cryptocurrency
      </Typography>
      </Box>
      <Box sx={{display:"flex", justifyContent:"space-evenly", padding:"0 60px", gap:"8px"}}>
        <CardTwo></CardTwo>
        <CardTwo></CardTwo>
        <CardTwo></CardTwo>
        <CardTwo></CardTwo>
      </Box>
      <Box sx={{display:"flex", padding:"10px 20px", margin:"8% 5%",borderColor:"white",borderStyle:"solid", borderRadius:"8px"}}>
        <Box sx={{padding:"10px 20px 10px 10px"}}>
        <Typography className="pa" variant="h5" gutterBottom>
        New In Cryptocurrency?
      </Typography>
      <Typography className="pa" variant="body1" gutterBottom>
      Well tell you what cryptocurrencies are, how they work and why you should own one right now. So lets do it.
      </Typography>
        </Box>
        <Box sx={{padding:"3%", margin:"0 0 0 90px"}}>
        <Button sx={{ display:"flex", justifyContent:"end", alignItems:"end", background:"#0FAE96"}} variant="contained">Learn & Explore Now</Button>
        </Box>
      </Box>
      <Box>
      <Box sx={{padding:"120px 60px 50px 60px", display:"flex", flexDirection:"column"}}>
      <Typography className="pa" variant="h4" gutterBottom>
      Market Update
      </Typography>
      <Typography className="pa" variant="body1" gutterBottom>
      Cryptocurrency Categories
      </Typography>
        <Box sx={{display:"flex", justifyContent:"space-between"}} >
          <Box sx={{display:"flex", gap:"10px"}}>
          <Button variant="contained">Popular</Button>
          <Button variant="contained">Metaverse</Button>
          <Button variant="contained">Entertainment</Button>
          <Button variant="contained">Energy</Button>
          <Button variant="contained">Gaming</Button>
          <Button variant="contained">Music</Button>
          <Button variant="contained">See All 12</Button>
          </Box >
          <Box sx={{gap:"0 60px 0 60px"}}>
          <Button  variant="contained">Search Coin</Button>
          </Box>
        </Box>
      </Box>
      
        <Box sx={{borderColor:"#ffff", borderRadius:"8px", borderStyle:"solid",display:"flex", flexDirection:"column",margin:"0 60px"}}>
            <Box sx={{display:"flex",justifyContent:"space-evenly",  padding:"0 60px"}}>
                <Typography className="pa" variant="h6">NO</Typography>
                <Typography className="pa" variant="h6">NAME</Typography>
                <Typography className="pa" variant="h6">LAST PRICE</Typography>
                <Typography className="pa" variant="h6">NCHANGE</Typography>
                <Typography className="pa" variant="h6">MARKET STATS</Typography>
                <Typography className="pa" variant="h6">TRADE</Typography>
                
            </Box>
            {pokemon?.results.map((item:any,index:number)=>{
        const splitUrl = item.url.split('/')
        const id = splitUrl[splitUrl.length - 2]
      return(
            <Grid key={index} item>
              <List
            id={id}
            image={`${process.env.NEXT_PUBLIC_IMAGES_POKE}/${id}.png`}
            name={item.name}
            text1="ded"
            text2="dewdew"
            ></List>
            </Grid>
          )
        })}
            
        </Box>
        
      </Box>
      <Box sx={{display:"flex", justifyContent:"space-between", padding:"80px 60px"}} >
      <Box sx={{display:"flex",flexDirection:"column" ,gap:"20px", padding:"20px"}}>
      <Typography className="pa" variant="h4" gutterBottom>
      How To Get Started
      </Typography>
      <Typography className="pa" variant="body2" gutterBottom >
      Simple and easy way to start your investment 
in cryptocurrency
      </Typography>
      <Box>
      <Button variant="contained">Get Started</Button>
      </Box>
      </Box>
      <Box sx={{display:"flex", justifyContent:"end", flexDirection:"column",gap:"8px"}}>
        <CardTree ></CardTree>
        <CardTree ></CardTree>
        <CardTree ></CardTree>
      </Box>
      </Box>
      <Box sx={{padding:"120px 0 50px 0", display:"flex", textAlign:"center", flexDirection:"column"}}>
      <Typography className="pa" variant="h4" gutterBottom sx={{padding:"60px 0 0 0"}}>
      CryptoCap Amazing Faetures
      </Typography>
      <Typography className="pa" variant="body1" gutterBottom sx={{padding:"0 0 60px 0"}}>
      Explore sensational features to prepare your best investment in cryptocurrency
      </Typography>
      <Grid sx={{display:"flex",justifyContent:"space-between", padding:"20px 60px"}} container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        {pokemon?.results.map((item:any,index:number)=>{
          const splitUrl = item.url.split('/')
          const id = splitUrl[splitUrl.length - 2]
          return(
          <Grid key={index} item >
            <CardFour
            image={`${process.env.NEXT_PUBLIC_IMAGES_POKE}/${id}.png`}
            title={item.name}
            text="dsd"
            ></CardFour>
          </Grid>
          )
        })}
      </Grid>
      <Box sx={{padding:"50px 60px"}}>
      <Link className="text2" href="#">See All Articles</Link>
      </Box>
      </Box>
      <Footer></Footer>
    </Box>
  );
}
