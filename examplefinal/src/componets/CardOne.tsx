import * as React from 'react';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import { Avatar } from '@mui/material';
import Button from '@mui/material/Button';
import { ArrowOutward } from '@mui/icons-material';
import AddIcon from '@mui/icons-material/Add';
import Fab from '@mui/material/Fab';

export default function CardOne() {
  return (
    <Card variant="outlined" sx={{ maxWidth:"none", width:"280px", background:"#fbf7f70d"}}>
      <Box sx={{display:'flex', justifyContent:"space-evenly", padding:"10px 20px"}}>
        <Avatar></Avatar>
        <Typography className='pa' variant="h6" gutterBottom>BTC</Typography>
        <Button sx={{padding:0}} variant="contained">BITCOIN</Button>
        <Fab size="small" color="primary" aria-label="add">
  <ArrowOutward />
</Fab>
      </Box>
      <Divider variant="middle" component="li"  />
      <Box sx={{ padding:"10px 20px"}}>
      <Typography className='pa' variant="button" display="block" gutterBottom>
      $56,623.54
      </Typography>
      <Typography className='pa' variant="caption" display="block" gutterBottom>
      1.41%
      </Typography>
      </Box>
    </Card>
  );
}
