import { Box } from "@mui/material"
import { Logo } from "./Logo"
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import Typography from "@mui/material/Typography";

export const Footer =()=>{
    return(
        <Box sx={{display:"flex", justifyContent:"space-between", padding:"0 60px", margin:"50px 0 0 0 "}}>
            <Box className="pa">
                <Logo></Logo>
                <Box>
                <InstagramIcon></InstagramIcon>
                <FacebookIcon></FacebookIcon>
                <TwitterIcon></TwitterIcon>
                <YouTubeIcon></YouTubeIcon>
                </Box>
                <Typography variant="body1"> 2021 CoinMarketCap. All rights reserved</Typography>
            </Box>
            <Box >
            <Typography sx={{color:"#ECF1F0"}} variant="h5">About Us</Typography>
            <Typography className="text2"  variant="h5">Aplications</Typography>
            <Typography className="text2" variant="h5">Careers</Typography>
            <Typography className="text2" variant="h5">Blog</Typography>
            <Typography className="text2" variant="h5">Legal & privacy</Typography>
            </Box>
            <Box>
            <Typography  sx={{color:"#ECF1F0"}} variant="h5">About Us</Typography>
            <Typography className="text2" variant="h5">Aplications</Typography>
            <Typography className="text2" variant="h5">Careers</Typography>
            <Typography className="text2" variant="h5">Blog</Typography>
            <Typography className="text2" variant="h5">Legal & privacy</Typography>
            </Box>
            <Box>
            <Typography  sx={{color:"#ECF1F0"}} variant="h5">About Us</Typography>
            <Typography className="text2" variant="h5">Aplications</Typography>
            <Typography className="text2" variant="h5">Careers</Typography>
            <Typography className="text2" variant="h5">Blog</Typography>
            <Typography className="text2" variant="h5">Legal & privacy</Typography>
            </Box>
        </Box>
    )
}