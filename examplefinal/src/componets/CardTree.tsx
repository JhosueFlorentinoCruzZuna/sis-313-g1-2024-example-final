import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import { CardActionArea } from '@mui/material';

export default function CardTree() {
  return (
    <Card sx={{ maxWidth: 360, height:"150px", background:"#fbf7f70d" }}>
      <CardActionArea sx={{display:"flex", padding:"10px"}}>
        <Avatar></Avatar>
        <CardContent>
          <Typography className='pa' gutterBottom variant="h6" component="div">
          Create Your Account
          </Typography>
          <Typography className='pa' variant="body2" color="text.secondary">
            Your account and personal identity are guaranteed safe.
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
