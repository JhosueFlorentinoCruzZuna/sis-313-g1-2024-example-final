import { Logo } from "./Logo";
import { Box } from "@mui/material"
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import Stack from '@mui/material/Stack';
import { SearchSharp } from "@mui/icons-material";

export const Nav=()=>{
    return(
        <Box className="pa" sx={{ display:"flex",justifyContent:"space-between", alignItems:"center", padding:"5px 60px"}}>
            <Logo></Logo>
            <Breadcrumbs separator=" " aria-label="breadcrumb">
        <Link className="pa" underline="hover"   href="/">
        Home
        </Link>
        <Link className="pa"
          underline="hover"
          color="inherit"
          href="/material-ui/getting-started/installation/"
        >
        Businesses
        </Link>
        <Link className="pa" underline="hover" color="inherit" href="/">
        Trade
        </Link>
        <Link className="pa" underline="hover" color="inherit" href="/">
        Market
        </Link>
        <Link className="pa" underline="hover" color="inherit" href="/">
        Learn
        </Link> 
      </Breadcrumbs>
        <Box>
        <Button sx={{background:"#0FAE96"}} variant="contained">Login</Button>
        </Box>
        </Box>
    )
}