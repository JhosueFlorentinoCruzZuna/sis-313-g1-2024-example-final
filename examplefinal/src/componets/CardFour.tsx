import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

interface po{
  image: string;
  title: string;
  text: string
}
export default function CardFour({image, title, text}:po) {
  const [expanded, setExpanded] = React.useState(false);
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
  return (
    <Card sx={{ width:"300px", height:"400px", background:"#fbf7f70d"}}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="250"
          image={image}
          alt="green iguana"
        />
        <CardContent sx={{display:'flex', flexDirection:"column", textAlign:"start"}}>
    
          <Typography className='pa' gutterBottom variant="h6" component="div">
            {title}
          </Typography>
          <Typography className='pa' variant="body1" color="text.secondary">
            {text}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
