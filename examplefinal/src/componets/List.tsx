import * as React from 'react';
import { Box } from "@mui/material"
import Typography from "@mui/material/Typography"
import Avatar from "@mui/material/Avatar"
import Button from "@mui/material/Button"
import Divider from "@mui/material/Divider"
import Image from 'next/image';
interface pok{
    id:string;
    image:string;
    name:string;
    text1:string;
    text2:string;
}
export const List =({id,image,name,text1,text2}:pok)=>{
    const [expanded, setExpanded] = React.useState(false);
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return(
        <Box sx={{ display:"flex", flexDirection:"column", padding:"0 60px"}}>
            
            <Divider component="li" color='#ECF1F0' />
            <Box sx={{display:"flex",justifyContent:"space-evenly"}}>
                <Box className="text"><Typography className="pa" variant="body1">{id}</Typography></Box>
                <Box className="text" sx={{display:"flex",justifyContent:"space-evenly"}}>
                <Avatar alt="Travis Howard" src={image} />
                <Typography className="pa" variant="body1">{name}</Typography>
                <Typography className="pa" variant="body1">/</Typography>
                <Typography className="pa" variant="body1">POKEMON</Typography>
                </Box>
                <Box className="text"><Typography className="pa" variant="body1">{text1}</Typography></Box>
                <Box className="text"><Typography className="pa" variant="body1">{text2}</Typography></Box>
                <Image src="/images/one.svg" alt='sd' width={80} height={80}></Image>
                <Box className="text">
                <Button variant="contained">Contained</Button>
                </Box>
            </Box>
        </Box>
    )
}