import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import { ArrowForward } from '@mui/icons-material';
export default function CardTwo() {
  return (
    <Card sx={{ maxWidth: 345, background:"#fbf7f70d"}}>
      <Box sx={{padding:"20px "}}>
      <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" sx={{ width: 56, height: 56 }} />
      </Box>
      <CardContent>
        <Typography className='pa' gutterBottom variant="h5" component="div">
        Manage Portfolio
        </Typography>
        <Typography className='pa' variant="body2" color="text.secondary">
        Buy and sell popular digital currencies, keep track of them in the one place.
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" endIcon={<ArrowForward />}>See Explained</Button>
      </CardActions>
    </Card>
  );
}
