import { getPokemon } from "@/services/Pokemon.service"
import { useEffect, useState } from "react"

export const usePokemon = () => {
    const [pokemon,setPokemon] = useState<any>()
    const fetchData = async () => {
        try{
            const responsePokemon = await getPokemon()
            setPokemon(responsePokemon)
        }catch(e){
            console.log(e)
        }
        
    }
    useEffect(() => {
        fetchData()
    },[])

    return {pokemon}
}
